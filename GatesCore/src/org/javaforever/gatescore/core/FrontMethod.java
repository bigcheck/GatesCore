package org.javaforever.gatescore.core;

import java.io.Serializable;
import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

import org.javaforever.gatescore.utils.StringUtil;

public class FrontMethod implements Comparable<FrontMethod>,Cloneable,Serializable{
	private static final long serialVersionUID = 6301709883042714777L;
	protected Long serial = 0L;
	protected int indent = 0;
	protected Set<FrontSignature> signatures = new TreeSet<FrontSignature>();
	protected String standardName;
	protected String content = "";
	protected String methodComment;
	protected StatementList methodStatementList = new StatementList();
	protected boolean noContainer = false;

	public Long getSerial() {
		return serial;
	}

	public void setSerial(Long serial) {
		this.serial = serial;
	}

	public int getIndent() {
		return indent;
	}

	public void setIndent(int indent) {
		this.indent = indent;
	}

	public boolean isNoContainer() {
		return noContainer;
	}

	public void setNoContainer(boolean noContainer) {
		this.noContainer = noContainer;
	}

	public String getMethodComment() {
		return methodComment;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public void setMethodComment(String methodComment) {
		this.methodComment = methodComment;
	}

	public String getStandardName() {
		return standardName;
	}

	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}
	
	public String getCapFirstMethodName() {
		return StringUtil.capFirst(standardName);
	}
	
	public String getLowerFirstMethodName() {
		return StringUtil.lowerFirst(standardName);
	}

	public String generateMethodString() {
		return "";
	}

	public String getThrowException() {
		return "Exception";
	}

	public StatementList getMethodStatementList() {
		this.methodStatementList.setSerial(this.serial);
		this.methodStatementList.setIndent(this.indent);
		return this.methodStatementList;
	}

	public void setMethodStatementList(StatementList methodStatementList){		
		this.methodStatementList = methodStatementList;
	}
	
	public String generateMethodContentStringWithSerial() {
		if (this.methodStatementList != null){
			StringBuilder sb = new StringBuilder();
			Collections.sort(this.methodStatementList);
			for (Statement s : this.methodStatementList){
				sb.append("\t\t"+s.getSerial()+"\t:\t"+s.getContent()+"\n");
			}
			return sb.toString();
		}
		else return this.content;
	}
		
	public String generateStandardCallString(){
		return "";
	}
	
	public String getStandardCallString(){
		return "";
	}

	@Override
	public int compareTo(FrontMethod o) {
		return this.standardName.compareTo(o.getStandardName());
	}
}
