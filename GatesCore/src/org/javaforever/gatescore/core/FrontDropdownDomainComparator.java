package org.javaforever.gatescore.core;

import java.io.Serializable;
import java.util.Comparator;

public class FrontDropdownDomainComparator implements Comparator<FrontDropdown>,Serializable{
	private static final long serialVersionUID = -1393441475502236748L;

	@Override
	public int compare(FrontDropdown o1, FrontDropdown o2) {
		return o1.getTarget().getCapFirstDomainName().compareTo(o2.getTarget().getCapFirstDomainName());
	}

}
