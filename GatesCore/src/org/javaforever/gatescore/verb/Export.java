package org.javaforever.gatescore.verb;

import org.javaforever.gatescore.core.FrontCodeBlock;
import org.javaforever.gatescore.core.FrontDomain;
import org.javaforever.gatescore.core.FrontMethod;
import org.javaforever.gatescore.core.FrontVerb;

public class Export extends FrontVerb{
	private static final long serialVersionUID = 1007425592831193320L;

	public Export(FrontDomain d) {
		super(d);	
		this.setVerbName("Export"+d.getCapFirstDomainName());
	}
	
	@Override
	public FrontMethod generateControllerMethod() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public FrontMethod generateApiMethod() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public FrontCodeBlock generateRouteBlockBlock() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
