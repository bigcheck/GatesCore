package org.javaforever.gatescore.verb;

import org.javaforever.gatescore.core.FrontCodeBlock;
import org.javaforever.gatescore.core.FrontDomain;
import org.javaforever.gatescore.core.FrontMethod;
import org.javaforever.gatescore.core.FrontVerb;

public class SearchByName extends FrontVerb {
	private static final long serialVersionUID = -9086636540841037015L;

	public SearchByName(FrontDomain d) {
		super(d);	
		this.setVerbName("SearchByName"+d.getCapFirstDomainName());
	}
	
	@Override
	public FrontMethod generateControllerMethod() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public FrontMethod generateApiMethod() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public FrontCodeBlock generateRouteBlockBlock() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
