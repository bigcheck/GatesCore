package com.javaforever.gatescore.gui;

public class TemplatesResourceCopyer {
	// 下载并解压拷贝的文件
	public void downloadUnpackTemplates(String projectFolderPath) throws Exception{
		 Resource res = new Resource();
			 res.downloadRes("/templates/FrontEndTemplates.zip", projectFolderPath+"/FrontEndTemplates.zip");
		 ZipCompressor compressor = new ZipCompressor();
		 compressor.unZipFilesAndDeleteOrginal(projectFolderPath+"/FrontEndTemplates.zip", projectFolderPath);
	}
}